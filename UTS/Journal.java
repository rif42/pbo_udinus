//RifkyAriyaPratama
//A11,2020.12628

public class Journal extends Book{
    Integer isbn;
    String type;

    Journal(String title, String author, String publisher, Integer pages, Integer isbn, String type) {
        super(title, author, publisher, pages);
        this.isbn = isbn;
        this.type = type;
    }

    public void getIsbn() {
        System.out.println("Journal: " + title + " isbn " + isbn);
    }

    public void getType() {
        System.out.println("Journal: " + title + " type " + type);
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    public void setType(String type) {
        this.type = type;
    }
}
