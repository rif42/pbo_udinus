//RifkyAriyaPratama
//A11,2020.12628

public class Item {
    String title;
    Boolean borrowed;
    Integer year, month, dayOfMonth;

    Item(String title) {
        this.title = title;
        this.borrowed = false;
    }

    public void getTime() {
        System.out.println("Item: " + title + " borrowed on " + dayOfMonth + "/" + month + "/" + year);
    }

    public void borrowDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = day;
        this.borrowed = true;   
    }

    public void print() {
        if (borrowed) {
            getTime();
        } else {
            System.out.println("Item: " + title + " not borrowed");
        }
    }
}
