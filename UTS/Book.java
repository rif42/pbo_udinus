//RifkyAriyaPratama
//A11,2020.12628


import java.util.GregorianCalendar;

public class Book extends Item{
    String author, publisher;
    Integer pages;
    Integer MAX_BORROW_DAY = 7;

    Book(String title, String author, String publisher, Integer pages) {
        super(title);
        this.author = author;
        this.publisher = publisher;
        this.pages = pages;
    }

    public void dueDate(GregorianCalendar borrowDate) {
        borrowDate.add(GregorianCalendar.DAY_OF_MONTH, MAX_BORROW_DAY);
    }
    
}
