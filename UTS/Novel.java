//RifkyAriyaPratama
//A11,2020.12628

public class Novel extends Book {

    Integer isbn;
    String genre;

    Novel(String title, String author, String publisher, Integer pages, Integer isbn, String genre) {
        super(title, author, publisher, pages);
        this.isbn = isbn;
        this.genre = genre;
    }

    public void getIsbn() {
        System.out.println("Novel: " + title + " isbn " + isbn);
    }

    public void getGenre() {
        System.out.println("Novel: " + title + " genre " + genre);
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
