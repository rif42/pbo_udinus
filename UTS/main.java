//RifkyAriyaPratama
//A11,2020.12628
public class main {

    public static void main(String args[]){
        Journal journal = new Journal("Red Rising", "Pierce Brown", "Goodreads", 523, 15839976, "Journal");

        System.out.println("Title: " + journal.title);
        System.out.println("Author: " + journal.author);
        System.out.println("Publisher: " + journal.publisher);
        System.out.println("ISBN: " + journal.isbn);

    }
}
