//RifkyAriyaPratama
//A11,2020.12628

public class DVD extends AudioVideo {
    String director;

    DVD(String title, String director, String studio) {
        super(title);
        this.director = director;
    }

    public void getDirector() {
        System.out.println("DVD: " + title + " directed by " + director);
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void print() {
        super.print();
        getDirector();
    }
}
