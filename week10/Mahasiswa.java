public class Mahasiswa extends Manusia {
    public int nim;
    public double ipk;

    public Mahasiswa(String nama, int umur, int tinggi_badan, int nim, double ipk) {
        super(nama, umur, tinggi_badan);
        this.nim = nim;
        this.ipk = ipk;
        System.out.println("DATA MAHASISWA");
    }

    public void dataDiri(){
        System.out.println("Nama Siswa: " + nama);
        System.out.println("Umur Siswa: " + umur);
        System.out.println("Tinggi Badan: " + tinggi_badan);
        System.out.println("NIM: " + nim);
    }

    public void buatTugas(){
        System.out.println("sedang membuat tugas..");
    }

    public void lulus(){
        System.out.println("Alhamdulillah lulus dengan ipk " + ipk);
    }
}
