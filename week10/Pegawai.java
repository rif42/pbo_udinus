public class Pegawai extends Manusia {
    public int nik;
    public String departemen;

    public Pegawai(String nama, int umur, int tinggi_badan, int nik, String departemen) {
        super(nama, umur, tinggi_badan);
        this.nik = nik;
        this.departemen = departemen;
        System.out.println("DATA PEGAWAI");
    }

    public void buatLaporan(){
        System.out.println("Nama Siswa: " + nama);
        System.out.println("Umur Siswa: " + umur);
        System.out.println("Tinggi Badan: " + tinggi_badan);
        System.out.println("NIK: " + nik);
        System.out.println("Departemen: " + departemen);
    }

    public void berangkatKerja(){
        System.out.println("sedang berangkat bekerja..");
    }

    public void pulangKerja(){
        System.out.println("sedang pulang bekerja..");
    }
}