public class Manusia {
    public String nama;
    public int umur;
    public int tinggi_badan;

    public Manusia(String nama, int umur, int tinggi_badan) {
        this.nama = nama;
        this.umur = umur;
        this.tinggi_badan = tinggi_badan;
    }

    public void lari() {
        System.out.println("sedang berlari..");
    }

    public void jalan() {
        System.out.println("sedang berjalan..");
    }

    public void makan() {
        System.out.println("sedang makan..");
    }

    public void tidur() {
        System.out.println("sedang tidur..");
    }

    public static void main(String[] args) {
        Manusia wibu = new Manusia("hizkia", 3000, 160);
        // wibu.nama = "hizkia";
        // wibu.umur = 3000;
        // wibu.tinggi_badan = 160;
        wibu.lari();
        System.out.println(wibu.nama);
    }
}